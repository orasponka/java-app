# JavaApp

A simple java app with login and more that I did a few years ago. <br />
May not work with all java versions. I recomend Java 11.

## How to run JavaApp

1. Clone this project
```
git clone https://gitlab.com/orasponka/java-app.git
```
2. Compile java scripts
```
cd java-app/JavaApp/Data/
javac scripts/*.java
mv scripts/*.class ./
```
3. Run App

Start app first time with command below
```
java FirstAcc
```
In future start JavaApp with command below 
```
java Login
```
