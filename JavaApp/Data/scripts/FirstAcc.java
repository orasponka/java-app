import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.Toolkit;
import java.awt.Image;
import java.security.NoSuchAlgorithmException;

public class FirstAcc implements ActionListener{
    boolean Kirjautunut = false;
    String KirjautunutNimi = "";
    String UlosKirjautunutNimi = "";

    private static JLabel userLabel;
    private static JTextField userText;
    private static JLabel passwordLabel;
    private static JPasswordField passwordText;
    private static JButton button;
    private static JLabel success;
    private static JButton button2;

    public void Acc(){
    

        JPanel panel = new JPanel();
        panel.setLayout(null);

        JFrame frame = new JFrame();
        frame.setSize(455, 190);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Create First Account");
        frame.add(panel);
        Image png = Toolkit.getDefaultToolkit().getImage("images/icon.png");
        frame.setIconImage(png);

        userLabel = new JLabel("Username:");
        userLabel.setBounds(20, 20, 80, 25);
        panel.add(userLabel);

        userText = new JTextField(20);
        userText.setBounds(100, 20, 165, 25);
        panel.add(userText);

        passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(20, 60, 80, 25);
        panel.add(passwordLabel);

        passwordText = new JPasswordField();
        passwordText.setBounds(100, 60, 165, 25);
        panel.add(passwordText);

        button = new JButton("Register");
        button.setBounds(300, 20, 120, 25);
        button.addActionListener(new FirstAcc());
        button.setBackground(Color.WHITE);
        button.setForeground(Color.GRAY);
        button.setBorder(new LineBorder(Color.BLACK));
        panel.add(button);

        button2 = new JButton("Login");
        button2.setBounds(300, 60, 120, 25);
        button2.addActionListener(new FirstAcc());
        button2.setBackground(Color.WHITE);
        button2.setForeground(Color.GRAY);
        button2.setBorder(new LineBorder(Color.BLACK));
        button2.setVisible(false);
        panel.add(button2);

        success = new JLabel("");
        success.setBounds(100, 120, 320, 25);
        panel.add(success);

        frame.setVisible(true);
    }
    
    public static void main(String[] args) {
        FirstAcc Ikkuna = new FirstAcc();
        Ikkuna.Acc();  
    }

    public void actionPerformed(ActionEvent e){
        if(e.getSource() == button){

            String[] Nimet = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
            String[] Salat = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};

            String user = userText.getText();
            String password = passwordText.getText();

            try {
                File myObj = new File("data/tilit.txt");
                int I = 0;
                int Toinen = 0;
                boolean Toinen2 = false;
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    Toinen++;
                    if(Toinen == 3){
                    I++;
                    Toinen = 0;
                    }
                    String data = myReader.nextLine();
                    if(Toinen2 == false){
                    Nimet[I] = data;
                    Toinen2 = true;
                    } else {
                        Toinen2 = false;
                        Salat[I] = data;
                    }
                }
                myReader.close();
            } catch (FileNotFoundException a){
                System.out.println("Virhe!!");
                a.printStackTrace();
            }

            int I;
            boolean Onnistui = true;
            hash h = new hash();
            try{
            for (I = 0; I < Nimet.length; I++){
            if (Nimet[I].equals(h.hashPassword(user)) || password.equals("")){
                success.setText("Register failed!");
                Onnistui = false;
            }
        } 
    }
    catch(NoSuchAlgorithmException ee){}

        if(Onnistui == true) {
            success.setText("Register successful!! Press Login Button to login");
            button2.setVisible(true);
            userText.setText("");
            passwordText.setText("");
            ViestitTiedosto Moi = new ViestitTiedosto();
            Moi.Write(user);
            Tili(user, password);
            try{
            FileWriter Kirjoitus = new FileWriter("Sovellus.cmd");
            Kirjoitus.write("java Login");
            Kirjoitus.close();
            } catch (IOException eee){}
            File Otakayttoon = new File("OtaKäyttöön.cmd");
            if(Otakayttoon.delete()){
                System.out.println("Onnistui");
            }
            File FirstAcc = new File("FirstAcc.class");
            if(FirstAcc.delete()){
                System.out.println("Onnistui");
            }
        }
    } else {
        if(e.getSource() == button2){
            Login LoginIkkuna = new Login();
            LoginIkkuna.LoginPage();
        }
    }
    }

    public void Tili(String message, String password){
        try{
        Append Kirj = new Append();
        hash h = new hash();
        Kirj.Append(h.hashPassword(message)+ "\n" + h.hashPassword(password), "data/tilit.txt", "Ei");
        System.out.println("Rekisteröinti Onnistui");
        }
        catch(NoSuchAlgorithmException e){}
    }
}