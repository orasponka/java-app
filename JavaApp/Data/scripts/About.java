import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.Image;

public class About{
    public void Page() { 
        JLabel label;
        JFrame frame;
        JPanel panel;

        frame = new JFrame();


        label = new JLabel("<html><h1>About</h1><br>This is beta application by Orri. <br>You can register to user and read your own notifycations sent by admin.</html>");
        label.setBounds(10, 40, 200, 200);
        label.setFont(new Font("Serif", Font.BOLD, 25));

        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));;
        panel.add(label);

        frame.add(panel, BorderLayout.CENTER);
        frame.setSize(400, 250);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("About");
        Image png = Toolkit.getDefaultToolkit().getImage("images/icon.png");
        frame.setIconImage(png);
        frame.pack();
        frame.setVisible(true);


    }
}