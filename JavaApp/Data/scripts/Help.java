import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.Image;

public class Help{
    public void Page() { 
        JLabel label;
        JFrame frame;
        JPanel panel;

        frame = new JFrame();


        label = new JLabel("<html><h1>Help</h1>When you launch the app for the first time, create a user. First user is Admin. Admin have access to Admin tools <br> Do not use a main password in this application. <br> Application administrators may delete your account at any time. <br> This is an unfinished application meaning not all things may work properly. <br> Regards Orri <br> <br> <h1>Privacy</h1><br> Names and Passwords are protected by the SHA-512 algorithm making it impossible to crack. The app is not connected to the internet so it cannot be hacked through it and app developers cannot access your data for anything.</html>");
        label.setBounds(10, 40, 200, 200);
        label.setFont(new Font("Serif", Font.BOLD, 25));

        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));;
        panel.add(label);

        frame.add(panel, BorderLayout.CENTER);
        frame.setSize(400, 250);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Help - Support");
        Image png = Toolkit.getDefaultToolkit().getImage("images/icon.png");
        frame.setIconImage(png);
        frame.pack();
        frame.setVisible(true);


    }
}