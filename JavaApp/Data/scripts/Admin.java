import javax.lang.model.element.Element;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.awt.Toolkit;
import java.awt.Image;

public class Admin implements ActionListener {
    boolean Kirjautunut = false;
    String KirjautunutNimi = "";
    String UlosKirjautunutNimi = "";
    Read Lue = new Read();
    String ValKirj = Lue.Read();

    private static JLabel userLabel;
    private static JTextField userrText;
    private static JLabel notifLabel;
    private static JTextField notifText;
    private static JButton button;
    private static JLabel resetLabel;
    private static JTextField resetText;
    private static JButton button2;
    private static JLabel success;
    private static JLabel btext;
    private static JButton button3;
    
    public static void Admin() {

        Read Lue = new Read();
        String ValKirj = Lue.Read();
        String Text = "";

        if(ValKirj == null || ValKirj == ""){
            Text = "Signed: None";
        } else {
            Text = "Signed: " + ValKirj;
        }

        System.out.println("Sovellus Avattu");
        System.out.println("Kirjautunut: " + ValKirj);
    

        JPanel panel = new JPanel();
        panel.setLayout(null);

        JFrame frame = new JFrame();
        frame.setSize(475, 325);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("Admin Tools");

        Image png = Toolkit.getDefaultToolkit().getImage("images/icon.png");
        frame.setIconImage(png);
        frame.add(panel);

        userLabel = new JLabel("User:");
        userLabel.setBounds(20, 20, 80, 25);
        panel.add(userLabel);

        userrText = new JTextField(20);
        userrText.setBounds(100, 20, 165, 25);
        panel.add(userrText);

        notifLabel = new JLabel("Notifycation:");
        notifLabel.setBounds(20, 60, 80, 25);
        panel.add(notifLabel);

        notifText = new JTextField(20);
        notifText.setBounds(100, 60, 165, 25);
        panel.add(notifText);

        button = new JButton("Send");
        button.setBounds(300, 20, 120, 25);
        button.addActionListener(new Admin());
        button.setBackground(Color.WHITE);
        button.setForeground(Color.GRAY);
        button.setBorder(new LineBorder(Color.BLACK));
        panel.add(button);

        resetLabel = new JLabel("User:");
        resetLabel.setBounds(20, 130, 80, 25);
        panel.add(resetLabel);

        resetText = new JTextField(20);
        resetText.setBounds(100, 130, 165, 25);
        panel.add(resetText);

        button2 = new JButton("Reset Notifs");
        button2.setBounds(300, 130, 120, 25);
        button2.addActionListener(new Admin());
        button2.setBackground(Color.WHITE);
        button2.setForeground(Color.GRAY);
        button2.setBorder(new LineBorder(Color.BLACK));
        panel.add(button2);

        btext = new JLabel("Reset Bulletin Board:");
        btext.setBounds(100, 200, 165, 25);
        panel.add(btext);

        button3 = new JButton("Reset");
        button3.setBounds(230, 200, 120, 25);
        button3.addActionListener(new Admin());
        button3.setBackground(Color.WHITE);
        button3.setForeground(Color.GRAY);
        button3.setBorder(new LineBorder(Color.BLACK));
        panel.add(button3);



        success = new JLabel("");
        success.setBounds(15, 255, 320, 25);
        panel.add(success);

        frame.setVisible(true);
    
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button){
            String user = userrText.getText();
            String message = notifText.getText();
            if(user.equals("") || message.equals("")){
                success.setText("Notifycation send failed!");
            } else {
                NotifSend Kirj = new NotifSend();
                Kirj.Send(message, user);
                success.setText("Notifycation sended!");
                userrText.setText("");
                notifText.setText("");
                System.out.println(user);
            }
        } else if(e.getSource() == button2){
            String user = resetText.getText();
            if(user.equals("")){
                success.setText("Reset failed. Submit user name plase.");
            } else {
                Reset vasara = new Reset();
                vasara.Reset(user);
                resetText.setText("");
                success.setText("User notifs are now reset!");
            }
        } else if(e.getSource() == button3){
            Reset2 Resettaaja = new Reset2();
            Resettaaja.Reset2();
            success.setText("Bulletin Board are now reset!");
        }
    }
}