import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.awt.Toolkit;
import java.awt.Image;


public class AdminAcc {
    public String Account(){
        String Nimi = "";
        try {
            File myObj = new File("data/tilit.txt");

            Scanner myReader = new Scanner(myObj);
            String data = myReader.nextLine();
            Nimi = data;
            myReader.close();
        } catch (FileNotFoundException a){
            System.out.println("Virhe!!");
            a.printStackTrace();
        }

        return Nimi;
    }
}