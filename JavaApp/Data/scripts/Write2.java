import java.io.FileWriter;
import java.io.IOException;

public class Write2 {
    public static void Write(String mitä){
        try {
            FileWriter myWriter = new FileWriter("data/tilit.txt");
            myWriter.write(mitä);
            myWriter.close();
            System.out.println("Kirjoittaminen onnistui");
        } catch (IOException e) {
            System.out.println("Virhe havaittu");
            e.printStackTrace();
        }
    }
}