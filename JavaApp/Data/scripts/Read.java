import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Read {
    public String Read(){
        String Kuka = "";
        try {
            File myObj = new File("data/kirj.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                Kuka = data;
            }
            myReader.close();
        } catch (FileNotFoundException e){
            System.out.println("Virhe!!");
            e.printStackTrace();
        }
        return Kuka;
    }
}