// Avaa kaikki tarvittavat paketit

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.Border;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.Color;

// Tästä alkaa Login Class

public class Login implements ActionListener {

    // Luon jo tässä kaikki tärkeät muuttujat

    boolean Kirjautunut = false;
    String KirjautunutNimi = "";
    String UlosKirjautunutNimi = "";
    Read Lue = new Read();
    String ValKirj = Lue.Read();

    AdminAcc Admin = new AdminAcc();
    String AdminK = Admin.Account();

    private static JLabel userLabel;
    private static JTextField userText;
    private static JLabel passwordLabel;
    private static JPasswordField passwordText;
    private static JButton button;
    private static JButton button2;
    private static JButton button3;
    private static JButton button4;
    private static JButton button5;
    private static JButton button6;
    private static JButton button7;
    private static JButton button8;
    private static JLabel success;
    private static JLabel kirj;

    // Tästä alkaa MainActivity eli asiat jota sovellus tekee automaattisesti

    public static void main(String[] args) {
        Login Ikkuna = new Login();
        Ikkuna.LoginPage();
    }

    public void LoginPage(){

        // Tämä kohta lukee kuka on kirjautunut jo valmiiksi

        Read Lue = new Read();
        String ValKirj = Lue.Read();
        String Text = "";

        if(ValKirj == null || ValKirj == ""){

            // Jos kukaan ei ole kirjautunut

            Text = "Signed: None";
        } else {

            /* Jos joku on kirjautunut sovellus katsoo onko sellainen käyttäjä olemassa jos on se kirjautuu sille. 
            Tätä on helppo kiertää jos muokkaa kirj.txt tiedostoa data kansiossa ja kirjoittaa olemassa oleven käyttäjän nimen. Näin voi kirjautua ilman salasanaa.*/

            String[] Nimet = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
            String[] Salat = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    
        
            try {
                File myObj = new File("data/tilit.txt");
                int I = 0;
                boolean Toinen2 = false;
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    if(Toinen2 == false){
                    Nimet[I] = data;
                    Toinen2 = true;
                    } else {
                        Toinen2 = false;
                        Salat[I] = data;
                        I++;
                    }
                }
                myReader.close();
            } catch (FileNotFoundException a){
                System.out.println("Virhe!!");
                a.printStackTrace();
            }
            Boolean totta = false;
            for (int i = 0; i < Nimet.length; i++){
                try{
                hash h = new hash();
                if(Nimet[i].equals(h.hashPassword(ValKirj))){
                    Text = "Signed: " + ValKirj;
                    totta = true;
                }
                } catch (NoSuchAlgorithmException error){}
                
            }
            // Jos käyttäjää ei ole oikeasti olemassa se ei kirjaudu
            if(!totta){
                Text = "Signed: None";
                Write kirjoitus = new Write();
                kirjoitus.Write("");
            }
        }

        System.out.println("Sovellus Avattu");
        System.out.println("Kirjautunut: " + ValKirj);

        // Alla luon kaikki ikkunan komponentit

        JPanel panel = new JPanel();
        panel.setLayout(null);

        JFrame frame = new JFrame();
        frame.setSize(575, 310);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Login - Mainpage");

        Image png = Toolkit.getDefaultToolkit().getImage("images/icon.png");
        frame.setIconImage(png);
        frame.add(panel);

        userLabel = new JLabel("Username:");
        userLabel.setBounds(20, 20, 80, 25);
        panel.add(userLabel);

        userText = new JTextField(20);
        userText.setBounds(100, 20, 165, 25);
        panel.add(userText);

        passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(20, 60, 80, 25);
        panel.add(passwordLabel);

        passwordText = new JPasswordField();
        passwordText.setBounds(100, 60, 165, 25);
        panel.add(passwordText);

        button = new JButton("Login / Logout");
        button.setBounds(300, 20, 120, 25);
        button.addActionListener(new Login());
        button.setBackground(Color.WHITE);
        button.setForeground(Color.GRAY);
        button.setBorder(new LineBorder(Color.BLACK));
        panel.add(button);

        button2 = new JButton("Creators");
        button2.setBounds(300, 60, 120, 25);
        button2.addActionListener(new Login());
        button2.setBackground(Color.WHITE);
        button2.setForeground(Color.GRAY);
        button2.setBorder(new LineBorder(Color.BLACK));
        panel.add(button2);

        button4 = new JButton("Notifycations");
        button4.setBounds(300, 140, 120, 25);
        button4.addActionListener(new Login());
        button4.setBackground(Color.WHITE);
        button4.setForeground(Color.GRAY);
        button4.setBorder(new LineBorder(Color.BLACK));
        panel.add(button4);

        button8 = new JButton("Bulletin Board");
        button8.setBounds(427, 140, 120, 25);
        button8.addActionListener(new Login());
        button8.setBackground(Color.WHITE);
        button8.setForeground(Color.GRAY);
        button8.setBorder(new LineBorder(Color.BLACK));
        panel.add(button8);

        button3 = new JButton("Register");
        button3.setBounds(300, 100, 120, 25);
        button3.addActionListener(new Login());
        button3.setBackground(Color.WHITE);
        button3.setForeground(Color.GRAY);
        button3.setBorder(new LineBorder(Color.BLACK));
        panel.add(button3);

        button5 = new JButton("Admin Tools");
        button5.setBounds(427, 20, 120, 25);
        button5.addActionListener(new Login());
        button5.setBackground(Color.WHITE);
        button5.setForeground(Color.GRAY);
        button5.setBorder(new LineBorder(Color.BLACK));
        panel.add(button5);

        button6 = new JButton("About");
        button6.setBounds(427, 60, 120, 25);
        button6.addActionListener(new Login());
        button6.setBackground(Color.WHITE);
        button6.setForeground(Color.GRAY);
        button6.setBorder(new LineBorder(Color.BLACK));
        panel.add(button6);

        button7 = new JButton("Help/Privacy");
        button7.setBounds(427, 100, 120, 25);
        button7.addActionListener(new Login());
        button7.setBackground(Color.WHITE);
        button7.setForeground(Color.GRAY);
        button7.setBorder(new LineBorder(Color.BLACK));
        panel.add(button7);

        success = new JLabel("");
        success.setBounds(15, 210, 320, 25);
        panel.add(success);

        kirj = new JLabel(Text);
        kirj.setBounds(15, 240, 320, 25);
        panel.add(kirj);

        frame.setVisible(true);

        // Jos kukaan ei ole kirjautunut Notifycations nappi ei näy

        ValKirj = Lue.Read();
        if(ValKirj == null || ValKirj == ""){
            button4.setVisible(false);
            button8.setVisible(false);
        } else {
            button4.setVisible(true);
            button8.setVisible(true);
        }
    
    }

    // Alla näkyy kaikkien nappien eventit eli mitä napit tekee

    public void actionPerformed(ActionEvent e) {

        // Tässä esimerkiksi Näkyy Kirjautumis napin eventti

        if (e.getSource() == button){
        System.out.println("Button");
        if(ValKirj == null || ValKirj == ""){
        if (Kirjautunut == false && KirjautunutNimi == "") {
        String user = userText.getText();
        String password = passwordText.getText();

        String[] Nimet = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
        String[] Salat = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};

    
        try {
            File myObj = new File("data/tilit.txt");
            int I = 0;
            boolean Toinen2 = false;
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if(Toinen2 == false){
                Nimet[I] = data;
                Toinen2 = true;
                } else {
                    Toinen2 = false;
                    Salat[I] = data;
                    I++;
                }
            }
            myReader.close();
        } catch (FileNotFoundException a){
            System.out.println("Virhe!!");
            a.printStackTrace();
        }

        int I;
        hash h = new hash();

        for (I = 0; I < Nimet.length; I++){
        try{
        if (Nimet[I].equals(h.hashPassword(user)) && Salat[I].equals(h.hashPassword(password))){
            if(Nimet[I] == "" || Salat[I] == ""){
                success.setText("Login Failed! Try Again!");
                System.out.println("Login Failed");
            } else {
            success.setText("Login successful! Welcome " + user + '!');
            Kirjautunut = true;
            KirjautunutNimi = user;
            I = Nimet.length;
            userText.setText("");
            passwordText.setText("");
            kirj.setText("Signed: " + KirjautunutNimi);
            Write Kirjoitus = new Write();
            Kirjoitus.Write(KirjautunutNimi);
            System.out.println("Login successful");
            button4.setVisible(true);
            button8.setVisible(true);
            }
        } else {
            success.setText("Login Failed! Try Again!");
            System.out.println("Login Failed");
            button4.setVisible(false);
            button8.setVisible(false);

        }
    }
    catch(NoSuchAlgorithmException ee){}
    }
    } else if (Kirjautunut == true && KirjautunutNimi != "") {
        UlosKirjautunutNimi = KirjautunutNimi;
        Kirjautunut = false;
        KirjautunutNimi = "";
        System.out.println("Ulos");
        success.setText("Logout successful! Goodbey " + UlosKirjautunutNimi + '!');
        kirj.setText("Signed: None");
        Write Kirjoitus = new Write();
        Kirjoitus.Write("");
        System.out.println("Logout successful");
        button4.setVisible(false);
        button8.setVisible(false);
    }
    } else {
        Kirjautunut = true;
        KirjautunutNimi = ValKirj;
        kirj.setText("Signed: " + KirjautunutNimi);
        ValKirj = "";
    }
    } else if (e.getSource() == button2) {
        Creators Moi = new Creators();
        Moi.Moi();
        System.out.println("Creators avattu");
    } else if(e.getSource() == button3){
        Rek rekk = new Rek();
        rekk.Rek();
        System.out.println("Rekisteröinti Avattu");
    } else if(e.getSource() == button4){
        Viestit notif = new Viestit();
        notif.Moi();
    } else if(e.getSource() == button5){
        hash salaus = new hash();
        ValKirj = Lue.Read();
        try{
        if(salaus.hashPassword(ValKirj).equals(AdminK)){
            Admin tools = new Admin();
            tools.Admin();
        } else {
            success.setText("You need Admin account!");
        }
    } catch (NoSuchAlgorithmException eee){}
    } else if(e.getSource() == button6){
        About sivu = new About();
        sivu.Page();
    } else if(e.getSource() == button7){
        Help page = new Help();
        page.Page();
    } else if(e.getSource() == button8){
        board Ilmoitustaulu = new board();
        Ilmoitustaulu.Moi();
    }
    }
}
